import React,{useState} from 'react';
import Board from './assets/Board';

export default function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const nextValue = currentMove % 2 === 0;
  const currentSquares = history[currentMove];

  function handlePlay(nextSquares) {
    setHistory(history.slice(0, currentMove + 1).concat([nextSquares]));
    setCurrentMove(prevMove => prevMove + 1);
  }

  function jumpTo(nextMove) {
    setCurrentMove(nextMove);
  }

  const moves = history.map((squares, move) => {
    const description = move ? 'Go to move #' + move : 'Go to game start';
    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    );
  });

  return (
    <div className="game">
      <Board nextValue={nextValue} squares={currentSquares} onPlay={handlePlay} />
      <ol>{moves}</ol>
    </div>
  );
}

